import "./blocks/_wp-block-vc-blocks-vctabs";
import "./blocks/_wp-block-cover";

(function ($) {
    $(document).scroll(function () {
        const $nav = $(".navbar.fixed-top");
        $nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height());
    });
})(jQuery);